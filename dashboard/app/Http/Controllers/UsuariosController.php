<?php

namespace App\Http\Controllers;

use App\Models\Setor;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UsuariosController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $usuarios = Usuario::paginate(25);
        return view('usuarios.index')->with('usuarios', $usuarios);
    }

    public function create()
    {
        $setores = Setor::all();
        return view('usuarios.create')->with('setores', $setores);
    }

    
    public function store(Request $request)
    {
        $this->validator($request);

        if (isset($request)) {
            $usuario = new Usuario;
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            //$usuario->password =  Hash::make($request->password);
            $usuario->password =  $request->password;
            $usuario->telefone = $request->telefone;
            $usuario->setor_id = $request->setor;
                        
            $usuario->save();
            return redirect()->route('usuarios.index')->with('message', 'Cadastrado realizado com sucesso!!!!!');
        } 
        else 
        {
            return redirect()->route('usuarios.index');
        }

    }

    public function edit($id)
    {           
        if (isset($id)) {
            $setores = Setor::all();
            $usuario = Usuario::find($id);
            return view('usuarios.create', ['usuario' => $usuario, 'setores' => $setores]);
        }

    }

    public function validator(Request $request){
        
        $regras = [
            'nome' => ['required', 'string', 'max:255', 'unique:usuarios'],
            'telefone' => ['required', 'string', 'max:15', 'min:8'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:usuarios'],
            'password' => ['required', 'string', 'min:6'],
        ];
        
        $mensagens = [
            'nome.required' => 'O campo nome é obrigatório',
            'email.required' => 'O campo e-mail é obrigatório',
            'email.unique' => 'Este endereço de e-mail já está cadastrado no sistema',
        ];

        $request->validate($regras, $mensagens);
    }

    public function update(Request $request, $id)
    {
        
        $this->validator($request);

        if (isset($request) && isset($id)) {
            $usuario = Usuario::find($id);      
            $usuario->nome = $request->nome;
            $usuario->email = $request->email;
            $usuario->password = $request->password;
            $usuario->telefone = $request->telefone;
            $usuario->setor_id = $request->setor;
            $usuario->save();            
        }
        
        return redirect()->route('usuarios.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Usuario::destroy($id);
        return redirect()->route('usuarios.index');
    }
    
}
