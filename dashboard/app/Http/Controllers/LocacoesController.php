<?php

namespace App\Http\Controllers;

use App\Models\Locacao;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Locacoescontroller extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $locacoes = Locacao::paginate(25);
        return view('locacoes.index')->with('locacoes', $locacoes);
    }

    public function create()
    {        
        return view('locacoes.create');
    }

    // LOCACOES=MODEL
    // 'usuario_id',
    // 'sala_id',    
    // 'data_reserva',
    // 'horario_reservado',

    public function locar(Request $request)
    {
        $this->validator($request);

        if (isset($request)) {
            $locacao = new Locacao;
            $locacao->usuario_id = $request->usuario_id;
            $locacao->horario_reservado = $request->horario_reservado;
            $locacao->data_reserva = $request->data_reserva;
        }
    }
    
    public function validator(Request $request){
        
        $regras = [
            'sala_id' => ['required', 'max:255',],
        ];
        
        $mensagens = [
            'sala_id.required' => 'O campo sala é obrigatório',
        ];

        $request->validate($regras, $mensagens);
    }

    public function store(Request $request)
    {
        $this->validator($request);

        if (isset($request)) {
            $locacao = new Locacao;
            $locacao->nome = $request->nome;
                                              
            $locacao->save();
            return redirect()->route('locacoes.index')->with('message', 'Cadastrado realizado com sucesso!!!!!');
        } 
        else 
        {
            return redirect()->route('locacoes.index');
        }

    }

    public function edit($id)
    {           
        if (isset($id)) {           
            $locacao = Locacao::find($id);
            return view('locacoes.create', ['locacao' => $locacao]);
        }

    }

    public function update(Request $request, $id)
    {
        $this->validator($request);
        
        if (isset($request) && isset($id)) {
            $locacao = Locacao::find($id);      
            $locacao->nome = $request->nome;
                       
            $locacao->save();            
        }
        
        return redirect()->route('locacoes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Locacao::destroy($id);
        return redirect()->route('locacoes.index');
    }
}
