<?php

namespace App\Http\Controllers;

use App\Models\Sala;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SalasController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $salas = Sala::paginate(25);
        return view('salas.index')->with('salas', $salas);
    }

    public function list_salas_api()
    {
        $salas = Sala::all();
        return response()->json($salas);
    }

    public function create()
    {        
        return view('salas.create');
    }

    public function store(Request $request)
    {
        $this->validator($request);
        
        if (isset($request)) {
            $sala = new Sala;
            $sala->nome = $request->nome;
            $sala->horario_inicio = $request->horario_inicio;
            $sala->horario_fim = $request->horario_fim;
                                   
            $sala->save();
            return redirect()->route('salas.index')->with('message', 'Cadastrado realizado com sucesso!!!!!');
        } 
        else 
        {
            return redirect()->route('salas.index');
        }

    }

    public function validator(Request $request){
        
        $regras = [
            'nome' => ['required', 'string', 'max:255', 'unique:usuarios'],
            'horario_inicio' => ['required', 'string', 'max:2', 'min:1'],
            'horario_fim' => ['required', 'string', 'max:2', 'min:1'],    
        ];
        
        $mensagens = [
            'nome.required' => 'O campo nome é obrigatório',
        ];

        $request->validate($regras, $mensagens);
    }

    public function edit($id)
    {           
        if (isset($id)) {           
            $sala = Sala::find($id);
            return view('salas.create', ['sala' => $sala]);
        }

    }

    public function update(Request $request, $id)
    {
        $this->validator($request);

        if (isset($request) && isset($id)) {
            $sala = Sala::find($id);      
            $sala->nome = $request->nome;
            $sala->horario_inicio = $request->horario_inicio;
            $sala->horario_fim = $request->horario_fim;
            
            $sala->save();            
        }
        
        return redirect()->route('salas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Sala::destroy($id);
        return redirect()->route('salas.index');
    }
}
