<?php

namespace App\Http\Controllers;

use App\Models\Setor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class SetoresController extends Controller
{
    //
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function list_setores_api()
    {
        $setor = Setor::all();
        return response()->json($setor);
    }

    public function index()
    {
        $setores = Setor::paginate(25);
        return view('setores.index')->with('setores', $setores);
    }

    public function create()
    {
        return view('setores.create');
    }

    public function validator(Request $request){

        $regras = [
            'nome' => ['required', 'string', 'max:255', 'unique:usuarios'],
        ];

        $mensagens = [
            'nome.required' => 'O campo nome é obrigatório',
        ];

        $request->validate($regras, $mensagens);
    }

    public function store(Request $request)
    {
        $this->validator($request);

        if (isset($request)) {
            $setor = new Setor;
            $setor->nome = $request->nome;

            $setor->save();
            return redirect()->route('setores.index')->with('message', 'Cadastrado realizado com sucesso!!!!!');
        }
        else
        {
            return redirect()->route('setores.index');
        }

    }

    public function edit($id)
    {
        if (isset($id)) {
            $setor = Setor::find($id);
            return view('setores.create', ['setor' => $setor]);
        }

    }

    public function update(Request $request, $id)
    {
        $this->validator($request);

        if (isset($request) && isset($id)) {
            $setor = Setor::find($id);
            $setor->nome = $request->nome;

            $setor->save();
        }

        return redirect()->route('setores.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $res = Setor::destroy($id);
        return redirect()->route('setores.index');
    }
}
