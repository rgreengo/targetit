<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Locacao extends Model
{
    use HasFactory;

    protected $table = 'locacoes';
    protected $primaryKey = 'id';

    protected $fillable = [
        'usuario_id',
        'sala_id',
        'data_reserva',
        'horario_reservado',
    ];

    function sala(){
        return $this->belongsTo('App\Models\Sala');
    }

    function usuario(){
        return $this->belongsTo('App\Models\usuario');
    }
}
