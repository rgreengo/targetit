<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('info');
});

Route::get('/home', function () {
    return view('welcome');
});



Auth::routes();

Route::resource('usuarios', 'UsuariosController');
Route::get('usuarios/delete/{usuario}', ['as' => 'usuarios.delete', 'uses' => 'UsuariosController@destroy']);

Route::resource('salas', 'SalasController');
Route::get('salas/delete/{sala}', ['as' => 'salas.delete', 'uses' => 'SalasController@destroy']);

Route::resource('setores', 'SetoresController');
Route::get('setores/delete/{setor}', ['as' => 'setores.delete', 'uses' => 'SetoresController@destroy']);

Route::resource('locacoes', 'LocacoesController');
Route::get('locacoes/delete/{locacao}', ['as' => 'locacoes.delete', 'uses' => 'LocacoesController@destroy']);

Route::get('/logout', 'Auth\LoginController@logout');