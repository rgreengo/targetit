<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Target IT</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Scripts -->
    <script src="{{ asset('js/jquery.js') }}" defer></script>
    <script src="{{ asset('js/bootstrap.js') }}" defer></script>
    <script src="{{ asset('js/app.js') }}" defer></script>    
    <script src="{{ asset('fontawesome-free/js/all.js') }}" defer></script>  

    <!-- Fonts -->    
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sb-admin-2.css') }}" rel="stylesheet">
    
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper" style="padding: 200px;">
        
        <!-- Content Wrapper -->
        <div id="content-wrapper">

            <!-- Main Content -->
            <div id="content">
                <h3 class="info h3" style="padding: 30px;">Bem vindo ao sistema de locação de salas da Target IT</h3>
                <br>
                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                    @if(isset(Auth::user()->name))
                        <div class="col-md-11 text-right">
                            <h5 class="info h5">Tu estas logado como: {{ Auth::user()->name }}</h5>
                        </div>                        
                        <div class="col-md-1 text-justify">
                            <h5 class="info h5">
                                <a href="{{ url('/logout') }}"> logout </a>
                            </h5>
                        </div>
                    @else
                    <div class="col-md-12 text-justify">
                        <h5 class="info h5">Neste momento tu não estas logado no sistema, para logar clique <a href="{{ url('/login') }}">aqui</a>                    
                        </h5>
                    </div>  
                    @endif
                </nav>
                <!-- End of Topbar -->
            </div>
        </div>
    </div>

</body>
</html>