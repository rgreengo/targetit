@extends('layouts.master')

@section('content')


    <div>        
        <div>
            <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec enim vitae turpis condimentum dignissim a quis odio. Proin tempus sollicitudin efficitur. Vivamus dictum sed tortor eget suscipit. Vivamus mauris purus, vestibulum in consectetur non, laoreet ut tortor. Suspendisse dapibus eu elit ac interdum. Sed non augue in nisl ultricies pretium. Quisque a nunc sollicitudin, blandit justo a, tempor neque.

Curabitur id urna magna. Phasellus laoreet fringilla tortor, at vestibulum nisi pharetra id. Vestibulum ut ex dui. Suspendisse metus nulla, tristique in ex nec, rutrum accumsan erat. Donec ac sollicitudin mi, et aliquet mi. Donec ut justo arcu. Proin et facilisis magna, in lacinia sapien. Phasellus porta sapien vitae lectus porttitor, sed faucibus est maximus. Nam at porttitor ligula. Phasellus hendrerit pretium metus, sed molestie sem accumsan vitae. Nullam turpis enim, vulputate at pretium ut, tempor quis justo. Vivamus fringilla posuere accumsan.

Maecenas et lacus eget dui vehicula mollis a a turpis. Cras et ex vulputate, efficitur ligula ut, tristique sapien. Curabitur id turpis non ante sollicitudin ultrices. Morbi eu consequat ipsum. Ut tristique, tellus nec rhoncus dapibus, justo libero imperdiet lectus, vitae cursus eros felis id lorem. Suspendisse aliquet iaculis vestibulum. Donec accumsan ante ac efficitur semper. Aliquam et libero non mauris volutpat tristique ac sed nunc. Morbi quis quam nec eros mollis mollis sed et magna. Suspendisse interdum nisl in sem convallis, id pellentesque nulla faucibus. Donec ultrices dolor tellus, vel interdum massa facilisis ut. Integer convallis dolor lorem.

<br>
<br>
<br>

Donec felis lorem, vestibulum vel justo non, congue euismod lacus. Fusce purus justo, volutpat a sapien a, commodo tempus ex. Mauris et efficitur est, quis ultrices sapien. Vivamus in malesuada elit, in suscipit purus. Ut nec tortor ac velit efficitur faucibus ut non leo. Nunc sagittis mi dui, non vestibulum quam molestie at. Praesent fringilla egestas metus, ut bibendum dolor sollicitudin vitae. In hac habitasse platea dictumst. Nulla pretium feugiat mi, at viverra lorem volutpat vel. Sed accumsan felis ac erat molestie aliquam.

Quisque pharetra dignissim purus tempus sollicitudin. Suspendisse nec bibendum libero. Aliquam id libero et risus congue consectetur. Duis elit arcu, cursus et felis eget, sodales vestibulum nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas neque mi, volutpat a velit ac, dapibus dapibus tortor. Integer congue pretium sem non interdum.

<br>
<br>
<br>

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla nec enim vitae turpis condimentum dignissim a quis odio. Proin tempus sollicitudin efficitur. Vivamus dictum sed tortor eget suscipit. Vivamus mauris purus, vestibulum in consectetur non, laoreet ut tortor. Suspendisse dapibus eu elit ac interdum. Sed non augue in nisl ultricies pretium. Quisque a nunc sollicitudin, blandit justo a, tempor neque.

Curabitur id urna magna. Phasellus laoreet fringilla tortor, at vestibulum nisi pharetra id. Vestibulum ut ex dui. Suspendisse metus nulla, tristique in ex nec, rutrum accumsan erat. Donec ac sollicitudin mi, et aliquet mi. Donec ut justo arcu. Proin et facilisis magna, in lacinia sapien. Phasellus porta sapien vitae lectus porttitor, sed faucibus est maximus. Nam at porttitor ligula. Phasellus hendrerit pretium metus, sed molestie sem accumsan vitae. Nullam turpis enim, vulputate at pretium ut, tempor quis justo. Vivamus fringilla posuere accumsan.

Maecenas et lacus eget dui vehicula mollis a a turpis. Cras et ex vulputate, efficitur ligula ut, tristique sapien. Curabitur id turpis non ante sollicitudin ultrices. Morbi eu consequat ipsum. Ut tristique, tellus nec rhoncus dapibus, justo libero imperdiet lectus, vitae cursus eros felis id lorem. Suspendisse aliquet iaculis vestibulum. Donec accumsan ante ac efficitur semper. Aliquam et libero non mauris volutpat tristique ac sed nunc. Morbi quis quam nec eros mollis mollis sed et magna. Suspendisse interdum nisl in sem convallis, id pellentesque nulla faucibus. Donec ultrices dolor tellus, vel interdum massa facilisis ut. Integer convallis dolor lorem.

Donec felis lorem, vestibulum vel justo non, congue euismod lacus. Fusce purus justo, volutpat a sapien a, commodo tempus ex. Mauris et efficitur est, quis ultrices sapien. Vivamus in malesuada elit, in suscipit purus. Ut nec tortor ac velit efficitur faucibus ut non leo. Nunc sagittis mi dui, non vestibulum quam molestie at. Praesent fringilla egestas metus, ut bibendum dolor sollicitudin vitae. In hac habitasse platea dictumst. Nulla pretium feugiat mi, at viverra lorem volutpat vel. Sed accumsan felis ac erat molestie aliquam.

Quisque pharetra dignissim purus tempus sollicitudin. Suspendisse nec bibendum libero. Aliquam id libero et risus congue consectetur. Duis elit arcu, cursus et felis eget, sodales vestibulum nibh. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas neque mi, volutpat a velit ac, dapibus dapibus tortor. Integer congue pretium sem non interdum.
            </div>
        </div>
    </div>

@stop