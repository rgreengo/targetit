@extends('layouts.master')

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Salas</h1>
    <script src="{{ asset('js/jquery.js') }}"></script>
    
    <a href="{{ url('salas') }}" class="btn btn-light btn-icon-split">
        <span class="icon text-gray-600">
            <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text">Voltar</span>
    </a>
    <div class="card-body" style="width: 90%;">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            @if(isset($sala))            
                {{ Form::open(array('method'=>'PUT', 'route' => ['salas.update', $sala->id])) }}    
            @else
                <form role="form" method="POST" action="{{ url('salas/') }}">
             @endif
                    {{ csrf_field() }}
                    <meta name="_token" content="{{ csrf_token() }}">
                    <div class="form-row">
                        <label for="nome" class="col-sm-12 col-form-label">Nome:</label>
                        <input class="form-control" type="text" name="nome" id="nome"
                            @if(isset($sala))value="{{$sala->nome}}" @endif>
                    </div>                 

                    <div class="form-row">
                        <label for="horario_inicio" class="col-sm-12 col-form-label">Horario Inicio:</label>
                        <input class="form-control" type="text" name="horario_inicio" id="horario_inicio"
                            @if(isset($sala))value="{{$sala->horario_inicio}}" @endif>
                    </div>           

                    <div class="form-row">
                        <label for="horario_fim" class="col-sm-12 col-form-label">Horario Fim:</label>
                        <input class="form-control" type="text" name="horario_fim" id="horario_fim"
                            @if(isset($sala))value="{{$sala->horario_fim}}" @endif>
                    </div>           

                    <div class="form-row" style="margin-top: 20px;">
                        <a href="{{ url('salas') }}" class="btn btn-dark btn-icon-split">
                            <span class="icon text-gray-600">
                                <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Cancelar</span>
                        </a>

                        <button style="margin-left: 10px;" class="btn btn-success btn-icon-split" type="submit">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">SALVAR</span>
                        </button>
                    </>
                @if(isset($sala))
                {{ Form::close() }}
                @else
                    </form>
                @endif            
        </table>
    </div>
    @if($errors->any())
    <div class="card-footer" style="width: 90%;">
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    </div>
    @endif
</div>
@endsection