@extends('layouts.master')

@section('content')
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="card-header">
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Salas</h1>
    <a href="{{ url('salas/create') }}" class="btn btn-success btn-icon-split">
      <span class="icon text-white-50">
        <i class="fa fa-plus-circle"></i>
      </span>
      <span class="text">Inserir nova sala</span>
    </a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Hora Início</th>
            <th>Hora Fim</th>
            <th style="width: 100px;">Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($salas as $sala)
          <tr>
            <td>{{ $sala->id }}</td>
            <td>{{ $sala->nome }}</td>
            <td>{{ $sala->horario_inicio }}</td>
            <td>{{ $sala->horario_fim }}</td>
            <td style="text-align: center;">
              <a href="{{ route('salas.delete', $sala->id) }}" class="jquery-postback btn btn-danger btn-circle btn-sm">
                <i class="fas fa-trash"></i>
              </a>

              <a href="salas/{{ $sala->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                <i class="fas fa-edit"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

  <div class="card-footer">
    <div class="pagination-lg justify-content-center">
      {{ $salas->links('vendor.pagination.custom') }}    
    </div>
  </div>  
</div>

@endsection