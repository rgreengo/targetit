@extends('layouts.master')

@section('content')
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="card-header">
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Locações</h1>
    <a href="{{ url('locacoes/create') }}" class="btn btn-success btn-icon-split">
      <span class="icon text-white-50">
        <i class="fa fa-plus-circle"></i>
      </span>
      <span class="text">Inserir nova locação</span>
    </a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Usuário</th>
            <th>Sala</th>
            <th>Data Reserva</th>
            <th>Horario Reserva</th>
            <th style="width: 100px;">Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($locacoes as $loc)
          <tr>
            <td>{{ $loc->id }}</td>
            <td>{{ $loc->usuario->nome }}</td>
            <td>{{ $loc->sala->nome }}</td>
            <td>{{ date("d/m/Y", strtotime($loc->data_reserva)) }}</td>
            <td>{{ $loc->horario_reservado }}</td>
            <td style="text-align: center;">
              <a href="{{ route('locacoes.delete', $loc->id) }}" class="jquery-postback btn btn-danger btn-circle btn-sm">
                <i class="fas fa-trash"></i>
              </a>

              <a href="locacoes/{{ $loc->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                <i class="fas fa-edit"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>   
  <div class="card-footer">
    <div class="pagination-lg justify-content-center">
      {{ $locacoes->links('vendor.pagination.custom') }}    
    </div>
  </div>  
</div>

@endsection