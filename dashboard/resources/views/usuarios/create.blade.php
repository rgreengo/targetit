@extends('layouts.master')

@section('content')
<main role="main">
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Usuarios</h1>
    <script src="{{ asset('js/jquery.js') }}"></script>
    
    <a href="{{ url('usuarios') }}" class="btn btn-light btn-icon-split">
        <span class="icon text-gray-600">
            <i class="fas fa-arrow-left"></i>
        </span>
        <span class="text">Voltar</span>
    </a>
    <div class="card-body" style="width: 90%;">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
            @if(isset($usuario))            
                {{ Form::open(array('method'=>'PUT', 'route' => ['usuarios.update', $usuario->id])) }}    
            @else
                <form role="form" method="POST" action="{{ url('usuarios/') }}">
             @endif
                    {{ csrf_field() }}
                    <meta name="_token" content="{{ csrf_token() }}">
                    <div class="form-row">
                        <label for="nome" class="col-sm-12 col-form-label">Nome:</label>
                        <input class="form-control" type="text" name="nome" id="nome"
                            @if(isset($usuario))value="{{$usuario->nome}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="telefone" class="col-sm-12 col-form-label">Telefone:</label>
                        <input class="form-control" type="text" name="telefone" id="telefone"
                            @if(isset($usuario))value="{{$usuario->telefone}}" @endif>
                    </div>
                    <div class="form-row">
                        <label for="email" class="col-sm-12 col-form-label">Email:</label>
                        <input class="form-control" type="text" name="email" id="email"
                            @if(isset($usuario))value="{{$usuario->email}}" @endif>
                    </div>                    
                    
                    <div class="form-row">
                        <label for="setor" class="col-sm-2 col-form-label">Setor:</label>
                        <select class="form-control" name="setor" id="setor">
                            @foreach($setores as $key => $value)
                            <option value="{{ $value->id }}">{{ $value->nome }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-row">
                        <label for="password" class="col-sm-12 col-form-label">Senha:</label>
                        <input class="form-control" type="password" name="password" id="password"
                            @if(isset($usuario))value="{{$usuario->password}}" @endif>
                    </div>                                                          

                    <div class="form-row" style="margin-top: 20px;">
                        <a href="{{ url('usuarios') }}" class="btn btn-dark btn-icon-split">
                            <span class="icon text-gray-600">
                                <i class="fas fa-arrow-left"></i>
                            </span>
                            <span class="text">Cancelar</span>
                        </a>

                        <button style="margin-left: 10px;" class="btn btn-success btn-icon-split" type="submit">
                            <span class="icon text-white-50">
                                <i class="fas fa-save"></i>
                            </span>
                            <span class="text">SALVAR</span>
                        </button>
                    </>
                @if(isset($usuario))
                {{ Form::close() }}
                @else
                    </form>
                @endif            
        </table>
    </div>
    @if($errors->any())
    <div class="card-footer" style="width: 90%;">
        @foreach ($errors->all() as $error)
        <div class="alert alert-danger" role="alert">
            {{ $error }}
        </div>
        @endforeach
    </div>
    @endif
</div>
</main>
@endsection