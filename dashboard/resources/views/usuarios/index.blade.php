@extends('layouts.master')

@section('content')
<div class="container-fluid">
  <!-- Page Heading -->
  <div class="card-header">
    <h1 class="h3 mb-2 text-gray-800">Cadastro/Edição Usuarios</h1>
    <a href="{{ url('usuarios/create') }}" class="btn btn-success btn-icon-split">
      <span class="icon text-white-50">
        <i class="fa fa-plus-circle"></i>
      </span>
      <span class="text">Inserir novo usuário</span>
    </a>
  </div>
  <div class="card-body">
    <div class="table-responsive">
      <table class="table table-bordered table-dark" id="dataTable" width="100%" cellspacing="0">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nome</th>
            <th>Telefone</th>
            <th>Email</th>
            <th>Setor</th>
            <th style="width: 100px;">Ações</th>
          </tr>
        </thead>

        <tbody>
          @foreach ($usuarios as $usu)
          <tr>
            <td>{{ $usu->id }}</td>
            <td>{{ $usu->nome }}</td>
            <td>{{ $usu->telefone }}</td>
            <td>{{ $usu->email }}</td>
            <td>{{ $usu->setor->nome }}</td>
            <td style="text-align: center;">
              <a href="{{ route('usuarios.delete', $usu->id) }}" class="jquery-postback btn btn-danger btn-circle btn-sm">
                <i class="fas fa-trash"></i>
              </a>

              <a href="usuarios/{{ $usu->id }}/edit" class="btn btn-primary btn-circle btn-sm">
                <i class="fas fa-edit"></i>
              </a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
  <div class="card-footer">
    <div class="pagination-lg justify-content-center">
      {{ $usuarios->links('vendor.pagination.custom') }}    
    </div>
  </div>  
</div>

@endsection