<?php

namespace Database\Factories;

use App\Models\Locacao;
use Illuminate\Database\Eloquent\Factories\Factory;

class LocacaoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Locacao::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'usuario_id' => $this->faker->numberBetween(10,20),
            'sala_id' => $this->faker->numberBetween(5,10),
            'horario_reservado' => $this->faker->numberBetween(8,18),
            'data_reserva' => $this->faker->date(),
        ];
    }     
}
