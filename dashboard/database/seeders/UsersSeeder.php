<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        $user = new User();
        $user->name = 'targetit-admin';
        $user->email = 'targetit@gmail.com';
        $user->password = 'targetit2020';
        $user->email_verified_at = now();
        $user->remember_token = '$2y$10$92IXUNp';
        $user->save();
    }
}
