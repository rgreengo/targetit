<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(SalasSeeder::class);
        $this->call(SetoresSeeder::class);
        $this->call(UsuariosSeeder::class);
        $this->call(LocacoesSeeder::class);
        $this->call(UsersSeeder::class);
    }
}
