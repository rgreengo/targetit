<?php

namespace Database\Seeders;

use App\Models\Locacao;
use Illuminate\Database\Seeder;

class LocacoesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {        
        Locacao::factory()->times(100)->create();
    }
}
